#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/stat.h>

#define MAX_EXT_LEN 10 // maksimal panjang ekstensi file
#define MAX_EXT_COUNT 100 // maksimal jumlah ekstensi yang disimpan pada file extensions.txt
#define MAX_FILE_COUNT 1000 // maksimal jumlah file yang diproses

int main() {
    // buat folder "categorized" jika belum ada
    char categorized_dir[] = "categorized";
    struct stat st = {0};
    if (stat(categorized_dir, &st) == -1) {
        mkdir(categorized_dir, 0700);
    }

    // baca daftar ekstensi dari file extensions.txt
    char *ext_list[MAX_EXT_COUNT];
    int ext_count = 0;
    char ext_filename[] = "extensions.txt";
    FILE *fp = fopen(ext_filename, "r");
    if (fp == NULL) {
        printf("Error: Failed to open file %s\n", ext_filename);
        return 1;
    }
    char line[MAX_EXT_LEN];
    while (fgets(line, MAX_EXT_LEN, fp) != NULL && ext_count < MAX_EXT_COUNT) {
        // hapus karakter newline
        if (line[strlen(line) - 1] == '\n')
            line[strlen(line) - 1] = '\0';

        // simpan ekstensi ke dalam array
        ext_list[ext_count] = malloc(strlen(line) + 1);
        strcpy(ext_list[ext_count], line);
        ++ext_count;
    }
    fclose(fp);

    // buat folder untuk setiap ekstensi di dalam folder "categorized"
    char ext_dir[MAX_EXT_LEN + 3]; // string untuk menampung nama folder ekstensi
    for (int i = 0; i < ext_count; ++i) {
        sprintf(ext_dir, "%s/%s", categorized_dir, ext_list[i]);
        if (stat(ext_dir, &st) == -1) {
            mkdir(ext_dir, 0700);
        }
    }

    // tambahkan folder "other" di dalam folder "categorized"
    char other_dir[] = "other";
    sprintf(ext_dir, "%s/%s", categorized_dir, other_dir);
    if (stat(ext_dir, &st) == -1) {
        mkdir(ext_dir, 0700);
    }

    // baca daftar file pada direktori saat ini
    struct dirent **namelist;
    int file_count = scandir(".", &namelist, NULL, alphasort);
    if (file_count < 0) {
        printf("Error: Failed to scan directory.\n");
        return 1;
    }

    // proses setiap file dan pindahkan/copy ke folder yang sesuai
    int max_file_count[MAX_EXT_COUNT] = {0};
    for (int i = 0; i < file_count; ++i) {
        // skip direktori dan file yang tidak memiliki ekstensi
        if (namelist[i]->d_type != DT_REG)
            continue;
        char *dot_ptr = strrchr(namelist[i]->d_name, '.');
        if (dot_ptr == NULL)
            continue;

        // ambil ekstensi dari file
        char ext[MAX_EXT_LEN];
        strcpy(ext, dot_ptr+1);

        // ubah semua huruf menjadi lowercase
        for (int j = 0; j < strlen(ext); ++j) {
            ext[j] = tolower(ext[j]);
        }

        // cari posisi ekstensi pada daftar ekstensi yang diberikan
        int ext_pos = -1;
        for (int j = 0; j < ext_count; ++j) {
            if (strcmp(ext, ext_list[j]) == 0) {
                ext_pos = j;
                break;
            }
        }
    // pindahkan/copy file sesuai dengan ekstensi
    char old_filename[MAX_EXT_LEN + 100];
    char new_filename[MAX_EXT_LEN + 100];

    if (ext_pos >= 0) {
        // ubah nama file menjadi lowercase dan tambahkan nomor urut jika sudah mencapai maksimum
        sprintf(old_filename, "%s", namelist[i]->d_name);
        sprintf(new_filename, "%s/%s", categorized_dir, ext_list[ext_pos]);

        if (max_file_count[ext_pos] >= MAX_FILE_COUNT) {
            // buat folder baru jika sudah mencapai jumlah file maksimum
            int folder_num = 2;
            char new_ext_dir[MAX_EXT_LEN + 15];
            do {
                sprintf(new_ext_dir, "%s (%d)", ext_list[ext_pos], folder_num++);
            } while (stat(new_ext_dir, &st) != -1);
            mkdir(new_ext_dir, 0700);
            sprintf(new_filename, "%s/%s", new_ext_dir, namelist[i]->d_name);
            max_file_count[ext_pos] = 0;
        }

        // cek apakah file yang sama sudah ada di dalam folder ekstensi
        struct dirent **namelist2;
        int file_count2 = scandir(ext_dir, &namelist2, NULL, alphasort);
        int file_exists = 0;
        for (int j = 0; j < file_count2; ++j) {
            if (strcmp(namelist2[j]->d_name, namelist[i]->d_name) == 0) {
                file_exists = 1;
                break;
            }
        }
        free(namelist2);

        // jika file belum ada, pindahkan/copy ke folder ekstensi
        if (!file_exists) {
            sprintf(new_filename, "%s/%s", new_filename, namelist[i]->d_name);
            if (rename(old_filename, new_filename) != 0) {
                printf("Error: Failed to move/copy file %s\n", old_filename);
            } else {
                ++max_file_count[ext_pos];
            }
        }
    } else {
        // pindahkan/copy file ke folder "other"
        sprintf(old_filename, "%s", namelist[i]->d_name);
        sprintf(new_filename, "%s/%s", categorized_dir, other_dir);

        // cek apakah file yang sama sudah ada di dalam folder "other"
        struct dirent **namelist2;
        int file_count2 = scandir(other_dir, &namelist2, NULL, alphasort);
        int file_exists = 0;
        for (int j = 0; j < file_count2; ++j) {
            if (strcmp(namelist2[j]->d_name, namelist[i]->d_name) == 0) {
                file_exists = 1;
                break;
            }
        }
        free(namelist2);

        // jika file belum ada, pindahkan/copy ke folder "other"
        if (!file_exists) {
            sprintf(new_filename, "%s/%s", new_filename, namelist[i]->d_name);
            if (rename(old_filename, new_filename) != 0) {
                printf("Error: Failed to move/copy file %s\n", old_filename);
            }
        }
    }

    free(namelist[i]);
}
free(namelist);

// bebaskan memori yang dialokasikan untuk daftar ekstensi
for (int i = 0; i < ext_count; ++i) {
    free(ext_list[i]);
}

return 0;

}
