#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
    // set the download URL and output filename
    char url[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char outfilename[] = "hehe.zip";

    // build the command to download the file using wget
    char command[1000];
    sprintf(command, "wget '%s' -O %s", url, outfilename);

    // execute the command
    system(command);

    // create a directory to store the unzip result
    char dirname[] = "heheunzip";
    sprintf(command, "mkdir %s", dirname);
    system(command);

    // build the command to extract the zip file with a specific output directory
    sprintf(command, "unzip %s -d %s", outfilename, dirname);

    // execute the command
    system(command);

    return 0;
}
