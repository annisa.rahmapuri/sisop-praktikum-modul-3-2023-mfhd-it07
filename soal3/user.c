#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>

#define QUEUE_KEY 1001
#define MAX_MESSAGE_SIZE 1024

struct message {
    long message_type;
    int user_id;
    char command[MAX_MESSAGE_SIZE];
};

int main() {
    int msg_queue_id;
    struct message msg;

    msg_queue_id = msgget(QUEUE_KEY, 0666 | IPC_CREAT);
    if (msg_queue_id == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    printf("Enter your user ID: ");
    scanf("%d", &msg.user_id);

    msg.message_type = 1;
    strcpy(msg.command, "CONNECT");
    if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) {
        perror("msgsnd");
        exit(EXIT_FAILURE);
    }

    while (1) {
        printf("> ");
        char command[MAX_MESSAGE_SIZE];
        scanf(" %[^\n]", command);

        if (strcmp(command, "EXIT") == 0) {
            strcpy(msg.command, "EXIT");
            if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) {
                perror("msgsnd");
                exit(EXIT_FAILURE);
            }
            break;
        } else {
            strncpy(msg.command, command, MAX_MESSAGE_SIZE);
            if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) {
                perror("msgsnd");
                exit(EXIT_FAILURE);
            }
        }
    }

    return 0;
}

//5027211019
