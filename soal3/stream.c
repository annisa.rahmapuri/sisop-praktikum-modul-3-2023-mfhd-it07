#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <ctype.h>
#include <stdint.h>
#include <jansson.h>
#include <sys/shm.h>
#include <semaphore.h>

#define SHM_KEY 1002
#define MAX_USERS 2
#define MAX_STREAMS 2
#define MAX_MESSAGE_SIZE 1024
#define QUEUE_KEY 1001

struct message {
    long message_type;
    int user_id;
    char command[MAX_MESSAGE_SIZE];
};

// Variabel global untuk menyimpan id shared memory
int shm_id;
sem_t user_semaphore;
sem_t playlist_semaphore;


// Deklarasikan fungsi yang diperlukan di sini
void rot13(char *input, char *output);
void hex_to_str(char *hex, char *str);
void base64_decode(const char *input, char *output);
void sort_playlist(char **playlist, size_t size);
void decrypt_and_sort();
void list_playlist();
void search_and_play(const char *query, int user_id);
void add_song_to_playlist(const char *song_title);

int main() {
    int msg_queue_id;
    struct message msg;

    msg_queue_id = msgget(QUEUE_KEY, 0666 | IPC_CREAT);
    if (msg_queue_id == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }
    shm_id = shmget(SHM_KEY, sizeof(int), 0666 | IPC_CREAT);
    if (shm_id == -1) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    int *user_count = (int *)shmat(shm_id, NULL, 0);
    if (user_count == (int *)-1) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    *user_count = 0;
    
      // Inisialisasi semaphore
    if (sem_init(&playlist_semaphore, 0, MAX_STREAMS) == -1) {
    perror("sem_init");
    exit(EXIT_FAILURE);
    }
    if (sem_init(&user_semaphore, 0, MAX_USERS) == -1) {
        perror("sem_init");
        exit(EXIT_FAILURE);
    }

   int *active_users = user_count; // Tambahkan baris ini untuk memperbaiki kesalahan

   while (1) {
        if (msgrcv(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0, 0) == -1) {
            perror("msgrcv");
            exit(EXIT_FAILURE);
        }

        if (strcmp(msg.command, "CONNECT") == 0) {
            if (sem_trywait(&user_semaphore) == 0) {
                (*active_users)++;
                printf("User %d connected\n", msg.user_id);
            } else {
                printf("USER SYSTEM OVERLOAD\n");
            }
        } else if (strcmp(msg.command, "EXIT") == 0) {
            (*active_users)--;
            sem_post(&user_semaphore); // Lepaskan semaphore pengguna
            if (*active_users == 0) {
                break;
            }
        
    } else if (strncmp(msg.command, "DECRYPT", 7) == 0) {
        decrypt_and_sort();
    } else if (strcmp(msg.command, "LIST") == 0) {
        list_playlist();
    } else if (strncmp(msg.command, "PLAY", 4) == 0) {
    if (sem_trywait(&playlist_semaphore) == 0) {
        char query[MAX_MESSAGE_SIZE];
        sscanf(msg.command, "PLAY \"%[^\"]\"", query);
        search_and_play(query, msg.user_id);
    } else {
        printf("STREAM SYSTEM OVERLOAD\n");
    }
    } else if (strncmp(msg.command, "ADD", 3) == 0) {
        char song_title[MAX_MESSAGE_SIZE];
        strncpy(song_title, msg.command + 4, strlen(msg.command) - 4);
        song_title[strlen(msg.command) - 4] = '\0';
        printf("User %d: ADD \"%s\"\n", msg.user_id, song_title);
        add_song_to_playlist(song_title);
    } else {
        printf("UNKNOWN COMMAND\n");
    }
}
    if (sem_destroy(&playlist_semaphore) == -1) {
    perror("sem_destroy");
    exit(EXIT_FAILURE);
    }

    if (msgctl(msg_queue_id, IPC_RMID, NULL) == -1) {
        perror("msgctl");
        exit(EXIT_FAILURE);
    }
    if (shmctl(shm_id, IPC_RMID, NULL) == -1) {
        perror("shmctl");
        exit(EXIT_FAILURE);
    }

    return 0;
}


void rot13(char *input, char *output) {
    for (int i = 0; input[i]; i++) {
        char c = input[i];
        if (isalpha(c)) {
            char offset = isupper(c) ? 'A' : 'a';
            c = ((c - offset + 13) % 26) + offset;
        }
        output[i] = c;
    }
}

void hex_to_str(char *hex, char *str) {
    size_t len = strlen(hex);
    for (size_t i = 0; i < len; i += 2) {
        sscanf(hex + i, "%2hhx", str + i / 2);
    }
}

void base64_decode(const char *input, char *output) {
    static const char decoding_table[] = {
        62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1,
        -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
        14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1,
        26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
        44, 45, 46, 47, 48, 49, 50, 51
    };
    size_t input_len = strlen(input);
    size_t output_len = input_len / 4 * 3;
    if (input[input_len - 1] == '=') output_len--;
    if (input[input_len - 2] == '=') output_len--;

    for (size_t i = 0, j = 0; i < input_len;) {
        uint32_t sextet_a = input[i] == '=' ? 0 & i++ : decoding_table[input[i++] - 43];
        uint32_t sextet_b = input[i] == '=' ? 0 & i++ : decoding_table[input[i++] - 43];
        uint32_t sextet_c = input[i] == '=' ? 0 & i++ : decoding_table[input[i++] - 43];
        uint32_t sextet_d = input[i] == '=' ? 0 & i++ : decoding_table[input[i++] - 43];

        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < output_len) output[j++] = (triple >> 2 * 8) & 0xFF;
                if (j < output_len) output[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < output_len) output[j++] = (triple >> 0 * 8) & 0xFF;
    }
    output[output_len] = '\0';
}

void sort_playlist(char **playlist, size_t size) {
// Use bubble sort to sort the playlist in alphabetical order
int swapped;
do {
swapped = 0;
for (size_t i = 0; i < size - 1; i++) {
if (strcmp(playlist[i], playlist[i + 1]) > 0) {
char *temp = playlist[i];
playlist[i] = playlist[i + 1];
playlist[i + 1] = temp;
swapped = 1;
}
}
} while (swapped);
}

void list_playlist() {
    FILE *playlist_file = fopen("playlist.txt", "r");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for reading\n");
        return;
    }

    char line[MAX_MESSAGE_SIZE];
    printf("Playlist:\n");
    while (fgets(line, MAX_MESSAGE_SIZE, playlist_file) != NULL) {
        printf("%s", line);
    }
    fclose(playlist_file);}

void decrypt_and_sort() {
    json_error_t error;
    json_t *root = json_load_file("song-playlist.json", 0, &error);
    if (!root) {
        fprintf(stderr, "Error parsing JSON file: %s\n", error.text);
        return; // Change this line
    }

size_t index;
json_t *data;
char **playlist = malloc(json_array_size(root) * sizeof(char *));
size_t playlist_size = 0;

json_array_foreach(root, index, data) {
    const char *method = json_string_value(json_object_get(data, "method"));
    const char *song = json_string_value(json_object_get(data, "song"));

    char decoded_song[1024] = {0};

    if (strcmp(method, "rot13") == 0) {
        rot13((char *)song, decoded_song);
    } else if (strcmp(method, "hex") == 0) {
        hex_to_str((char *)song, decoded_song);
    } else if (strcmp(method, "base64") == 0) {
        base64_decode(song, decoded_song);
    } else {
        fprintf(stderr, "Unknown method: %s\n", method);
        continue;
    }

    playlist[playlist_size] = strdup(decoded_song);
    playlist_size++;
}

sort_playlist(playlist, playlist_size);

 FILE *playlist_file = fopen("playlist.txt", "w");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for writing\n");
        json_decref(root);
        free(playlist);
        return; // Change this line
    }

for (size_t i = 0; i < playlist_size; i++) {
    fprintf(playlist_file, "%s\n", playlist[i]);
    free(playlist[i]);
}

fclose(playlist_file);
json_decref(root);
free(playlist);
}

void search_and_play(const char *query, int user_id){
    FILE *playlist_file = fopen("playlist.txt", "r");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for reading\n");
        return;
    }

    // Mengubah query menjadi lowercase
    char query_lower[MAX_MESSAGE_SIZE];
    for (int i = 0; query[i]; i++) {
        query_lower[i] = tolower(query[i]);
    }
    query_lower[strlen(query)] = '\0';

    char line[MAX_MESSAGE_SIZE];
    int match_count = 0;
    size_t line_number = 0;
    char matched_line[MAX_MESSAGE_SIZE];

    printf("Searching for \"%s\"...\n", query_lower);
    while (fgets(line, MAX_MESSAGE_SIZE, playlist_file) != NULL) {
        line_number++;

        // Mengubah judul lagu menjadi lowercase
        char line_lower[MAX_MESSAGE_SIZE];
        for (int i = 0; line[i]; i++) {
            line_lower[i] = tolower(line[i]);
        }
        line_lower[strlen(line)] = '\0';

        if (strstr(line_lower, query_lower) != NULL) {
            match_count++;
            strncpy(matched_line, line, MAX_MESSAGE_SIZE);
            printf("%zu. %s", line_number, line);
        }
    }

    int *active_users = (int *)shmat(shm_id, NULL, 0);
    if (active_users == (int *)-1) {
        perror("shmat");
        return;
    }

    if (match_count == 0) {
        printf("There is no song containing \"%s\"\n", query_lower);
    } else if (match_count == 1) {
    printf("User %d playing \"%s\"", user_id, matched_line);
    sleep(5); // Simulasikan durasi lagu, gantilah dengan kode yang sebenarnya untuk memutar lagu
    sem_post(&playlist_semaphore);  // Lepaskan semaphore
    }else {
        printf("There are %d songs containing \"%s\":\n", match_count, query_lower);
    }

    if (shmdt(active_users) == -1) {
        perror("shmdt");
        return;
    }

    fclose(playlist_file);
}


void add_song_to_playlist(const char *song_title) {
    FILE *playlist_file = fopen("playlist.txt", "a");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for appending\n");
        return;
    }

    fprintf(playlist_file, "%s\n", song_title);
    fclose(playlist_file);
}
