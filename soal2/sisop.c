#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>   // Library untuk clock()

#define ROWS1 4
#define COLS2 5

// Fungsi untuk menghitung faktorial
long double factorial(int n) {
   long double result = 1.0L;
   int i;
   for (i = 1; i <= n; i++) {
      result *= (long double)i;
   }
   return result;
}

int main() {
   int shmid;
   key_t key = 1234;

   // Attach shared memory
   int (*shared_result)[COLS2];
   if ((shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666)) < 0) {
      perror("shmget");
      exit(1);
   }
   if ((shared_result = shmat(shmid, NULL, 0)) == (int (*)[COLS2]) -1) {
      perror("shmat");
      exit(1);
   }

   // Tampilkan matriks hasil perkalian
   printf("Matriks hasil perkalian:\n");
   int i, j;
   for (i = 0; i < ROWS1; i++) {
      for (j = 0; j < COLS2; j++) {
         printf("%d ", shared_result[i][j]);
      }
      printf("\n");
   }

   // Hitung faktorial setiap angka dalam matriks tanpa thread
   long double factorial_value;
   printf("\nHasil faktorial setiap angka dalam matriks (tanpa thread):\n");
   clock_t start = clock();                  // Waktu mulai penghitungan faktorial
   for (i = 0; i < ROWS1; i++) {
      for (j = 0; j < COLS2; j++) {
         factorial_value = factorial(shared_result[i][j]);
         printf("%.0Lf ", factorial_value);
      }
      printf("\n");
   }
   clock_t end = clock();                    // Waktu selesai penghitungan faktorial
   double elapsed_time = (double)(end - start) / CLOCKS_PER_SEC;   // Waktu eksekusi dalam detik

   // Tampilkan waktu eksekusi
   printf("\nWaktu eksekusi (tanpa thread): %.6lf detik\n", elapsed_time);

   // Detach shared memory
   if (shmdt(shared_result) == -1) {
      perror("shmdt");
      exit(1);
   }

   return 0;
}
