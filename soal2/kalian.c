#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5

int main() {
   int matrix1[ROWS1][COLS1];
   int matrix2[ROWS2][COLS2];
   int result[ROWS1][COLS2];
   int shmid;
   key_t key = 1234;

   // Inisialisasi matriks pertama dengan angka random antara 1-5
   srand(time(NULL));
   int i, j;
   for (i = 0; i < ROWS1; i++) {
      for (j = 0; j < COLS1; j++) {
         matrix1[i][j] = rand() % 5 + 1;
      }
   }

   // Inisialisasi matriks kedua dengan angka random antara 1-4
   for (i = 0; i < ROWS2; i++) {
      for (j = 0; j < COLS2; j++) {
         matrix2[i][j] = rand() % 4 + 1;
      }
   }

   // Perkalian matriks
   for (i = 0; i < ROWS1; i++) {
      for (j = 0; j < COLS2; j++) {
         result[i][j] = 0;
         int k;
         for (k = 0; k < COLS1; k++) {
            result[i][j] += matrix1[i][k] * matrix2[k][j];
         } 
      }
   }
   
   // Buat shared memory
   if ((shmid = shmget(key, sizeof(result), IPC_CREAT | 0666)) < 0) {
      perror("shmget");
      exit(1);
   }

   // Attach shared memory
   int (*shared_result)[COLS2];
   if ((shared_result = shmat(shmid, NULL, 0)) == (int (*)[COLS2]) -1) {
      perror("shmat");
      exit(1);
   }

   // Simpan hasil perkalian pada shared memory
   for (i = 0; i < ROWS1; i++) {
      for (j = 0; j < COLS2; j++) {
         shared_result[i][j] = result[i][j];
      }
   }

   // Detach shared memory
   shmdt(shared_result);
   
    // mencetak matriks pertama
    printf("Matriks Pertama:\n");
    for(int i=0; i<4; i++) {
        for(int j=0; j<2; j++) {
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }

    // mencetak matriks kedua
    printf("\nMatriks Kedua:\n");
    for(int i=0; i<2; i++) {
        for(int j=0; j<5; j++) {
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }
    
    // mencetak matriks hasil perkalian
    printf("\nHasil Perkalian:\n");
    for(int i=0; i<4; i++) {
        for(int j=0; j<5; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
   
   return 0;
}
