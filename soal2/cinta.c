#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>

#define ROWS1 4
#define COLS2 5

// Fungsi untuk menghitung faktorial
long double factorial(int n) {
   long double result = 1.0L;
   int i;
   for (i = 1; i <= n; i++) {
      result *= (long double)i;
   }
   return result;
}

// Struktur data untuk informasi thread
struct thread_info {
   pthread_t thread_id;              // ID thread
   int row;                          // Baris matriks
   int col;                          // Kolom matriks
   int value;                        // Nilai di baris dan kolom tersebut
   long double factorial_value;      // Hasil faktorial dari nilai tersebut
};

// Fungsi thread untuk menghitung faktorial
void *calculate_factorial(void *arg) {
   struct thread_info *info = arg;
   info->factorial_value = factorial(info->value);
   return NULL;
}

int main() {
   int shmid;
   key_t key = 1234;

   // Attach shared memory
   int (*shared_result)[COLS2];
   if ((shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666)) < 0) {
      perror("shmget");
      exit(1);
   }
   if ((shared_result = shmat(shmid, NULL, 0)) == (int (*)[COLS2]) -1) {
      perror("shmat");
      exit(1);
   }

   // Tampilkan matriks hasil perkalian
   printf("Matriks hasil perkalian:\n");
   int i, j;
   for (i = 0; i < ROWS1; i++) {
      for (j = 0; j < COLS2; j++) {
         printf("%d ", shared_result[i][j]);
      }
      printf("\n");
   }

   // Hitung faktorial setiap angka dalam matriks menggunakan thread
   struct thread_info thread_data[ROWS1 * COLS2];  // Informasi thread
   int num_threads = 0;                            // Jumlah thread

   clock_t start = clock();                        // Catat waktu mulai penghitungan
   for (i = 0; i < ROWS1; i++) {
      for (j = 0; j < COLS2; j++) {
         thread_data[num_threads].row = i;
         thread_data[num_threads].col = j;
         thread_data[num_threads].value = shared_result[i][j];
         pthread_create(&thread_data[num_threads].thread_id, NULL, calculate_factorial, &thread_data[num_threads]);
         num_threads++;
      }
   }

   // Tunggu thread selesai
   for (i = 0; i < num_threads; i++) {
      pthread_join(thread_data[i].thread_id, NULL);
   }

   clock_t end = clock();                          // Catat waktu selesai penghitungan
   double cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;  // Waktu eksekusi dalam detik

   // Tampilkan hasil faktorial setiap angka pada matriks
   printf("\nHasil faktorial setiap angka dalam matriks:\n");
   for (i = 0; i < ROWS1; i++) {
      for (j = 0; j < COLS2; j++) {
         printf("%.0Lf ", thread_data[i*COLS2+j].factorial_value);
      }
      printf("\n");
   }

   // Detach shared memory
   if (shmdt(shared_result) == -1) {
      perror("shmdt");
      exit(1);
   }

   printf("\nWaktu eksekusi: %.6f detik\n", cpu_time_used);   // Tampilkan waktu eksekusi

   return 0;
}
