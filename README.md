# Laporan Praktikum Sistem Operasi - Modul 3

## Soal 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut!


**(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).**
1. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
2. Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
3. Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
4. Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi.
5. Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.


   **Catatan:**
   - Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh:
      - Huruf A   : 01000001
      - Huruf a   : 01100001
   - Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
   - Agar lebih mudah, ubah semua huruf kecil ke huruf kapital


### **1**.**0**
Kami membuat Algoritma Huffman untuk proses kompresi lossless. Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).
- Import beberapa library dan pendefinisian beberapa konstanta.
   
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/wait.h>


    #define MAX_SIZE 256


    ```
    - library/header file di-importkan menggunakan #include seperti stdio.h, stdlib.h, string.h, unistd.h, dan sys/types.h.
- Mendefinisan algoritma Huffman.
   
    ```c
    // Structure for a node in Huffman Tree
    typedef struct Node {
    char character;
    int frequency;
    struct Node* left;
    struct Node* right;
    } Node;
    ```
    - Struktur data Node memiliki empat anggota yaitu character yang bertipe char dan menyimpan karakter dari node, frequency yang bertipe int dan menyimpan frekuensi kemunculan karakter tersebut, serta dua anggota yang bertipe pointer yaitu left dan right yang merepresentasikan anak kiri dan anak kanan dari node tersebut.
- Mendefinisan Node dalam Huffman Tree.
   
    ```c
    // Function to create a new node for Huffman Tree
    Node* createNode(char character, int frequency) {
    Node* node = (Node*)malloc(sizeof(Node));
    node->character = character;
    node->frequency = frequency;
    node->left = NULL;
    node->right = NULL;
    return node;
    }


    ```
    - Fungsi createNode memiliki dua parameter yaitu character yang bertipe char dan frequency yang bertipe int. Fungsi ini akan mengalokasikan memori dengan ukuran sebesar sizeof(Node) menggunakan fungsi malloc dan menyimpan alamat memori tersebut ke dalam pointer node.


    - Selanjutnya, fungsi akan mengisi nilai dari anggota-anggota node seperti character, frequency, left, dan right dengan nilai yang diberikan pada saat pemanggilan fungsi, dan mengembalikan nilai pointer node sebagai node baru pada pohon Huffman.


### **1**.**1**
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
-  Menghitung frekuensi kemunculan huruf pada file.
   
    ```c
    // Function to count the frequency of each character in the file
    void countCharacterFrequency(FILE* file, int frequencies[]) {
    int ch;
    while ((ch = fgetc(file)) != EOF) {
        if (ch >= 'A' && ch <= 'Z') {
            frequencies[ch]++;
        }
      }
    }
    ```
    - Fungsi countCharacterFrequency memiliki dua parameter yaitu file yang bertipe pointer ke FILE dan frequencies yang bertipe array integer. Fungsi ini menggunakan loop while untuk membaca setiap karakter dari file menggunakan fungsi fgetc.

    - Selama pembacaan karakter, jika karakter yang dibaca merupakan huruf kapital (A-Z), maka nilai pada frequencies pada indeks yang sesuai dengan karakter tersebut akan ditambah satu.

### **1**.**2**
Melakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
-  Membuat Huffman Tree dari frekuensi karakter.
   
    ```c
    // Function to build Huffman Tree from character frequencies
    Node* buildHuffmanTree(int frequencies[]) {
    // Create a min-heap (priority queue) of nodes
    Node* heap[MAX_SIZE];
    int heapSize = 0;

    for (int i = 0; i < MAX_SIZE; i++) {
        if (frequencies[i] > 0) {
            heap[heapSize++] = createNode(i, frequencies[i]);
        }
    }

    // Build the Huffman Tree
    while (heapSize > 1) {
        // Extract the two nodes with minimum frequency
        Node* left = heap[--heapSize];
        Node* right = heap[--heapSize];

        // Create a new internal node with combined frequency
        Node* internalNode = createNode('\0', left->frequency + right->frequency);
        internalNode->left = left;
        internalNode->right = right;

        // Insert the new node back into the min-heap
        int i = heapSize;
        while (i > 0 && internalNode->frequency < heap[i - 1]->frequency) {
            heap[i] = heap[i - 1];
            i--;
        }
        heap[i] = internalNode;
        heapSize++;
    }

      // Return the root of the Huffman Tree
      return heap[0];
    }

    ```
    - Fungsi buildHuffmanTree memiliki satu parameter yaitu frequencies yang bertipe array integer dan merepresentasikan frekuensi kemunculan setiap karakter.

    - Pada awalnya, fungsi akan membuat sebuah min-heap (priority queue) menggunakan array heap yang memiliki ukuran maksimum MAX_SIZE. Selanjutnya, fungsi akan membuat node baru untuk setiap karakter yang memiliki frekuensi kemunculan lebih dari nol menggunakan fungsi createNode, dan menyimpannya pada min-heap.

    - Selanjutnya, fungsi akan membangun pohon Huffman dengan cara mengambil dua node dengan frekuensi terkecil dari min-heap, membuat sebuah node internal baru dengan frekuensi yang merupakan jumlah dari kedua node tersebut, dan menghubungkan node tersebut sebagai anak kiri dan anak kanan dari node internal baru. Node internal baru kemudian dimasukkan kembali ke dalam min-heap. Langkah ini diulang sampai hanya tersisa satu node di dalam min-heap yang merupakan akar dari pohon Huffman.

-  Melakukan kompresi file dengan menggunakan algoritma Huffman.
   
    ```c
    // Function to write Huffman Tree to compressed file in text format
    void writeHuffmanTreeToFile(Node* root, FILE* file) {
    if (root == NULL)
        return;

    if (root->left == NULL && root->right == NULL) {
        fprintf(file, "1%c", root->character);  // Leaf node indicator
    } else {
        fprintf(file, "0");  // Internal node indicator
        writeHuffmanTreeToFile(root->left, file);
        writeHuffmanTreeToFile(root->right, file);
      }
    }

    ```
    - Fungsi writeHuffmanTreeToFile memiliki dua parameter yaitu root yang merupakan pointer ke node akar dari pohon Huffman dan file yang merupakan pointer ke file yang akan ditulis.

    - Fungsi akan melakukan pengecekan jika root adalah NULL, maka fungsi akan mengembalikan void. Selanjutnya, jika root merupakan leaf node, maka fungsi akan menuliskan "1" dan karakter root->character ke dalam file. Sedangkan jika root merupakan internal node, maka fungsi akan menuliskan "0" ke dalam file, kemudian memanggil dirinya sendiri dengan parameter node anak kiri dan node anak kanan dari root.

### **1**.**3**
Pada child process, simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
-  Convert character ke Huffman dengan mengirim melalui pipe.
   
    ```c
    // Function to convert character to Huffman code and send through pipe in text format
    void convertToHuffmanCode(Node* root, char character, char* code, int depth, FILE* pipeWrite) {
    if (root == NULL)
        return;

        if (root->left == NULL && root->right == NULL && root->character == character) {
        code[depth] = '\0';  // Null-terminate the code
        fprintf(pipeWrite, "%s", code);  // Send Huffman code through pipe
        return;
    }

    code[depth] = '0';
    convertToHuffmanCode(root->left, character, code, depth + 1, pipeWrite);

    code[depth] = '1';
    convertToHuffmanCode(root->right, character, code, depth + 1, pipeWrite);
    }

    ```
    - Fungsi convertToHuffmanCode memiliki lima parameter yaitu root yang merupakan pointer ke node akar dari pohon Huffman, character yang merupakan karakter yang akan dikonversi menjadi kode Huffman, code yang merupakan buffer untuk menyimpan kode Huffman yang dihasilkan, depth yang merupakan kedalaman saat ini pada pohon Huffman, dan pipeWrite yang merupakan pointer ke pipa untuk menulis data.

    - Fungsi ini menggunakan pendekatan rekursif untuk melakukan traversal pada pohon Huffman. Fungsi akan memeriksa apakah root NULL, jika ya maka fungsi akan mengembalikan void. Kemudian, jika root merupakan leaf node dan root->character sama dengan character yang ingin dikonversi, maka kode Huffman yang disimpan pada buffer code akan diakhiri dengan karakter nul dan dikirim melalui pipa menggunakan fungsi fprintf.

    - Jika root bukan leaf node, maka fungsi akan memanggil dirinya sendiri secara rekursif dengan parameter node anak kiri dari root dan menambahkan karakter '0' pada buffer code. Kemudian, fungsi akan memanggil dirinya sendiri lagi dengan parameter node anak kanan dari root dan menambahkan karakter '1' pada buffer `code'.

### **1**.**4**
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi.
-  Membaca Huffman Tree lalu melakukan dekompresi.
   
    ```c
    // Read Huffman Tree from compressed file
        FILE* compressedFileRead = fopen("compressed.txt", "r");
        if (compressedFileRead == NULL) {
            printf("Error opening compressed file for reading.\n");
            return 1;
        }
        Node* decompressedHuffmanTree = buildHuffmanTreeFromFile(compressedFileRead);
        fclose(compressedFileRead);

        // Open the compressed file for reading
        FILE* compressedFileRead2 = fopen("compressed.txt", "r");
        if (compressedFileRead2 == NULL) {
            printf("Error opening compressed file for reading.\n");
            return 1;
        }

        // Perform decompression and write to the decompressed file
        FILE* decompressedFile = fopen("decompressed.txt", "w");
        if (decompressedFile == NULL) {
            printf("Error opening decompressed file for writing.\n");
            return 1;
        }
        decompressFile(compressedFileRead2, decompressedFile, decompressedHuffmanTree);
        fclose(compressedFileRead2);
        fclose(decompressedFile);

    ```
    - Kode membaca file terkompresi, membangun Pohon Huffman darinya menggunakan fungsi buildHuffmanTreeFromFile(), dan kemudian menggunakan Pohon Huffman itu untuk melakukan dekompresi file. Ini membuka file terkompresi lagi, lalu membuka file baru untuk menulis output yang didekompresi. Fungsi decompressFile()dipanggil dengan file terkompresi, file keluaran, dan Pohon Huffman sebagai argumen. Akhirnya, file terkompresi dan keluaran ditutup.

### **1**.**5**
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
-  Menghitung jumlah bit setelah dilakukan kompresi.
   
    ```c
    // Function to count the number of bits in a file (in text format)
    int countBitsInFile(FILE* file) {
    int count = 0;
    int ch;

    while ((ch = fgetc(file)) != EOF) {
        count += 8;  // Assuming 8 bits per character
    }

    return count;
    }

    // Function to count the number of bits in Huffman-compressed file (in text format)
    int countBitsInCompressedFile(FILE* file) {
    int count = 0;
    char code[MAX_SIZE];
    int depth = 0;
    int ch;

    while ((ch = fgetc(file)) != EOF) {
        if (ch == '0' || ch == '1') {
            code[depth++] = ch;
        } else if (ch == '\n') {
            code[depth] = '\0';  // Null-terminate the code
            count += strlen(code);
            depth = 0;
        }
    }

    return count;
    }

    ```
    - Fungsi pertama countBitsInFiledigunakan untuk menghitung jumlah bit dalam file dalam format teks. Itu membaca setiap karakter file dan mengasumsikan bahwa setiap karakter mewakili 8 bit, dan menambah penghitung sebesar 8 untuk setiap karakter.

    - Fungsi kedua countBitsInCompressedFiledigunakan untuk menghitung jumlah bit dalam file terkompresi Huffman dalam format teks. Itu membaca setiap karakter file dan jika '0' atau '1', itu menambahkannya ke buffer kode sementara. Jika menemukan karakter baris baru ('\n'), null-mengakhiri buffer kode, menambahkan panjangnya ke hitungan, dan me-reset buffer. Proses ini diulangi hingga akhir file tercapai. Fungsi ini mengasumsikan bahwa setiap baris file terkompresi mewakili kode Huffman untuk karakter, dan dengan demikian panjang kode sama dengan jumlah bit yang digunakan untuk mewakili karakter tersebut dalam file terkompresi.

## Output Soal 1

### **lossless.c**
![Alt Text](https://i.ibb.co/Yc8kJwB/Screenshot-2023-05-13-191020.png)

## Soal 2

Fajar sedang *sad* karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa *ngoding* dan tidak bisa menghitung, jadi Fajar memohon *jokian* kalian. Tugas Fajar adalah sebagai berikut.

1. Membuat program C dengan nama **kalian.c**, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam *code*. Matriks nantinya akan berisi angka *random* dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.
2. Buatlah program C kedua dengan nama **cinta.c**. Program ini akan mengambil variabel hasil perkalian matriks dari program **kalian.c** (program sebelumnya). Tampilkan hasil matriks tersebut ke layar.

**(Catatan: wajib menerapkan konsep shared memory)**

3. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

**Contoh:**

array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …

**(Catatan: Wajib menerapkan *thread* dan *multithreading* dalam penghitungan faktorial)**

4. Buatlah program C ketiga dengan nama **sisop.c**. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada **cinta.c** namun tanpa menggunakan *thread* dan *multithreading*. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan *multithread* dengan yang tidak.

### **2**.**1**

Kami diminta untuk membuat program C dengan nama **kalian.c**, yang berisi program untuk membuat matriks sesusai dengan perintah soal dan melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam *code*. Matriks nantinya akan berisi angka *random* dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

- Import beberapa library dan pendefinisian beberapa konstanta.
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <sys/ipc.h>
    #include <sys/shm.h>
    #include <time.h>
    
    #define ROWS1 4
    #define COLS1 2
    #define ROWS2 2
    #define COLS2 5
    ```
    
    - Pada bagian ini adalah inclusion atau penggunaan library yang dibutuhkan, yaitu `stdio.h`, `stdlib.h`, `sys/ipc.h`, `sys/shm.h`, dan `time.h`. Selain itu juga terdapat definisi konstanta untuk ukuran matriks.
- Proses Pembuatan Matriks
    
    ```c
    int main() {
       int matrix1[ROWS1][COLS1];
       int matrix2[ROWS2][COLS2];
       int result[ROWS1][COLS2];
       int shmid;
       key_t key = 1234;
    
       // Inisialisasi matriks pertama dengan angka random antara 1-5
       srand(time(NULL));
       int i, j;
       for (i = 0; i < ROWS1; i++) {
          for (j = 0; j < COLS1; j++) {
             matrix1[i][j] = rand() % 5 + 1;
          }
       }
    
       // Inisialisasi matriks kedua dengan angka random antara 1-4
       for (i = 0; i < ROWS2; i++) {
          for (j = 0; j < COLS2; j++) {
             matrix2[i][j] = rand() % 4 + 1;
          }
       }
    
       // Perkalian matriks
       for (i = 0; i < ROWS1; i++) {
          for (j = 0; j < COLS2; j++) {
             result[i][j] = 0;
             int k;
             for (k = 0; k < COLS1; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
             } 
          }
       }
    ```
    
    - Pada fungsi `main()`, terdapat pembuatan tiga buah matriks yaitu `matrix1`, `matrix2`, dan `result`. Selanjutnya, dilakukan inisialisasi pada masing-masing elemen matriks `matrix1` dan `matrix2` dengan angka random antara 1-5 dan 1-4 menggunakan fungsi `rand()` yang di-generate berdasarkan waktu saat ini dengan menggunakan fungsi `srand(time(NULL))`.
    - Kemudian, melakukan perkalian matriks pada `matrix1` dan `matrix2` dengan cara nested looping pada indeks baris dan kolom masing-masing matriks. Hasil perkalian disimpan pada matriks `result`.
- Attach shared memory
    
    ```c
    // Buat shared memory
       if ((shmid = shmget(key, sizeof(result), IPC_CREAT | 0666)) < 0) {
          perror("shmget");
          exit(1);
       }
    
       // Attach shared memory
       int (*shared_result)[COLS2];
       if ((shared_result = shmat(shmid, NULL, 0)) == (int (*)[COLS2]) -1) {
          perror("shmat");
          exit(1);
       }
    
       // Simpan hasil perkalian pada shared memory
       for (i = 0; i < ROWS1; i++) {
          for (j = 0; j < COLS2; j++) {
             shared_result[i][j] = result[i][j];
          }
       }
    
       // Detach shared memory
       shmdt(shared_result);
    ```
    
    - Proses pembuatan shared memory menggunakan fungsi `shmget` dengan parameter key `1234`, ukuran memori sebesar `sizeof(result)`, serta flags `IPC_CREAT | 0666`. Selanjutnya, attach shared memory menggunakan fungsi `shmat` dengan parameter id dari shared memory, NULL, dan flags 0. Jika terjadi error dalam meng-attach shared memory, maka program akan keluar.
    - Setelah itu, hasil perkalian pada matriks `result` disimpan pada shared memory menggunakan nested looping pada indeks baris dan kolom masing-masing matriks. Terakhir, detach shared memory dengan menggunakan fungsi `shmdt`.
    
    ```c
    // mencetak matriks pertama
        printf("Matriks Pertama:\n");
        for(int i=0; i<4; i++) {
            for(int j=0; j<2; j++) {
                printf("%d ", matrix1[i][j]);
            }
            printf("\n");
        }
    
        // mencetak matriks kedua
        printf("\nMatriks Kedua:\n");
        for(int i=0; i<2; i++) {
            for(int j=0; j<5; j++) {
                printf("%d ", matrix2[i][j]);
            }
            printf("\n");
        }
        
        // mencetak matriks hasil perkalian
        printf("\nHasil Perkalian:\n");
        for(int i=0; i<4; i++) {
            for(int j=0; j<5; j++) {
                printf("%d ", result[i][j]);
            }
            printf("\n");
        }
    ```
    
    - Matriks hasil perkalian yang telah disimpan pada variabel `result` tidak dicetak pada program utama. Sebaliknya, matriks pertama dan kedua, serta matriks hasil perkalian yang disimpan pada shared memory dicetak menggunakan fungsi `printf`.

### **2**.**2** - **2**.**3**

Kami diminta untuk membuatlah program C kedua dengan nama **cinta.c**. Program ini akan mengambil variabel hasil perkalian matriks dari program **kalian.c** (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

- Import beberapa library dan pendefinisian beberapa konstanta.
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <sys/ipc.h>
    #include <sys/shm.h>
    #include <pthread.h>
    #include <time.h>
    
    #define ROWS1 4
    #define COLS2 5
    ```
    
    - Program di atas membutuhkan beberapa library untuk dapat berjalan dengan baik, yaitu `stdio.h` (untuk fungsi input/output standar), `stdlib.h` (untuk alokasi memory dinamis dan fungsi umum lainnya), `sys/ipc.h` dan `sys/shm.h` (untuk shared memory), `pthread.h` (untuk thread), dan `time.h` (untuk mengukur waktu eksekusi) serta beberapa konstanta.
- Membuat fungsi factorial
    
    ```c
    long double factorial(int n) {
       long double result = 1.0L;
       int i;
       for (i = 1; i <= n; i++) {
          result *= (long double)i;
       }
       return result;
    }
    ```
    
    - Fungsi ini merupakan fungsi untuk menghitung faktorial dengan input sebuah bilangan bulat positif `n`. Faktorial didefinisikan sebagai perkalian semua bilangan bulat positif dari 1 hingga n. Fungsi ini mengembalikan hasil faktorial dalam tipe data `long double`.
- Menginisialisasi struktur data thread_info
    
    ```c
    struct thread_info {
       pthread_t thread_id;              // ID thread
       int row;                          // Baris matriks
       int col;                          // Kolom matriks
       int value;                        // Nilai di baris dan kolom tersebut
       long double factorial_value;      // Hasil faktorial dari nilai tersebut
    };
    ```
    
    - Struktur data ini menyimpan informasi mengenai sebuah thread, yaitu ID thread, baris dan kolom pada matriks, nilai pada baris dan kolom tersebut, dan hasil faktorial dari nilai tersebut.
- Membuat fungsi untuk menghitung nilai faktorial
    
    ```c
    void *calculate_factorial(void *arg) {
       struct thread_info *info = arg;
       info->factorial_value = factorial(info->value);
       return NULL;
    }
    ```
    
    - Fungsi ini merupakan fungsi yang akan dieksekusi oleh setiap thread untuk menghitung faktorial dari nilai pada matriks. Fungsi ini menerima argumen sebuah pointer ke struktur data `thread_info`, kemudian melakukan penghitungan faktorial menggunakan fungsi `factorial()`. Setelah itu, hasil faktorial disimpan kembali ke dalam struktur data `thread_info`.
- Attach shared memory
    
    ```c
    int main() {
       int shmid;
       key_t key = 1234;
    
       // Attach shared memory
       int (*shared_result)[COLS2];
       if ((shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666)) < 0) {
          perror("shmget");
          exit(1);
       }
       if ((shared_result = shmat(shmid, NULL, 0)) == (int (*)[COLS2]) -1) {
          perror("shmat");
          exit(1);
       }
    ```
    
    - Pertama-tama, program akan mencoba meng-attach shared memory dengan menggunakan fungsi `shmget()`. Jika berhasil, program akan meng-attach shared memory tersebut ke variable `shared_result` menggunakan fungsi `shmat()`.
- Menghitung nilai faktorial
    
    ```c
    // Tampilkan matriks hasil perkalian
    printf("Matriks hasil perkalian:\n");
    int i, j;
    for (i = 0; i < ROWS1; i++) {
       for (j = 0; j < COLS2; j++) {
          printf("%d ", shared_result[i][j]);
       }
       printf("\n");
    }
    ```
    
    - Setelah itu, program akan menampilkan matriks hasil perkalian ke layar menggunakan perulangan for.
    
    ```c
    struct thread_info thread_data[ROWS1 * COLS2];  // Informasi thread
    int num_threads = 0;
    
    clock_t start = clock();
    ```
    
    - Program akan mulai menghitung faktorial setiap angka dalam matriks dengan menggunakan thread. Untuk itu, program membuat sebuah array `thread_data` berisi informasi mengenai setiap thread yang akan dibuat.
    - Program juga memperoleh waktu awal sebelum penghitungan dimulai menggunakan fungsi `clock()` yang akan digunakan sebagai komparasi kinerja antara program yang menggunakan thread maupun tidak
    
    ```c
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS2; j++) {
            thread_data[num_threads].row = i;
            thread_data[num_threads].col = j;
            thread_data[num_threads].value = shared_result[i][j];
            pthread_create(&thread_data[num_threads].thread_id, NULL, calculate_factorial, &thread_data[num_threads]);
            num_threads++;
        }
    }
    ```
    
    - For loop ini akan membentuk setiap thread untuk menghitung faktorial dari setiap angka pada matriks. Setiap thread diberikan nilai baris dan kolom, nilai pada baris dan kolom tersebut, serta alamat dari struktur data `thread_info` yang sesuai. Fungsi `pthread_create()` kemudian akan membuat thread baru menggunakan fungsi `calculate_factorial()`.
    
    ```c
    // Tunggu thread selesai
    for (i = 0; i < num_threads; i++) {
       pthread_join(thread_data[i].thread_id, NULL);
    }
    ```
    
    - Setelah semua thread selesai, program akan menunggu setiap thread selesai bekerja menggunakan fungsi `pthread_join()`. Kemudian, program memperoleh waktu akhir setelah penghitungan selesai dan menghitung waktu eksekusi program.
    
    ```c
    clock_t end = clock();
    double cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
    
    printf("\nWaktu eksekusi: %.6f detik\n", cpu_time_used);   // Tampilkan waktu eksekusi
    
    // Tampilkan hasil faktorial setiap angka pada matriks
    printf("\nHasil faktorial setiap angka dalam matriks:\n");
    for (i = 0; i < ROWS1; i++) {
        for (j = 0; j < COLS2; j++) {
            printf("%.0Lf ", thread_data[i*COLS2+j].factorial_value);
        }
        printf("\n");
    }
    ```
    
    - Setelah semua thread selesai, kita catat waktu selesai penghitungan dan menampilkan waktu eksekusi program dalam detik.
    - Program ini akan menampilkan hasil faktorial dari setiap angka pada matriks ke layar menggunakan perulangan for. Program juga mencetak waktu eksekusi program menggunakan fungsi `printf()`.
    
    ```c
    // Detach shared memory
       if (shmdt(shared_result) == -1) {
          perror("shmdt");
          exit(1);
       }
    
       printf("\nWaktu eksekusi: %.6f detik\n", cpu_time_used);   // Tampilkan waktu eksekusi
    
       return 0;
    }
    ```
    
    - Bagian ini digunakan untuk melepas shared memory yang sudah di-attach sebelumnya dan menampilkan waktu eksekusi total dari program dalam detik.

### **2**.**4**

Kami diminta untuk membuat program C ketiga dengan nama **sisop.c** yang bertujuan sama dengan program sebelumnya ****namun tanpa menggunakan *thread* dan *multithreading*. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan *multithread* dengan yang tidak. Indikator pembeda antara kedua program 

- Import beberapa library dan pendefinisian beberapa konstanta.
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <sys/ipc.h>
    #include <sys/shm.h>
    #include <time.h>  
    
    #define ROWS1 4
    #define COLS2 5
    ```
    
    - Bagian tesebut adalah kumpulan library yang dibutuhkan oleh program ini. `stdio.h` digunakan untuk input-output standar seperti `printf` dan `scanf`, `stdlib.h` digunakan untuk alokasi memori dinamis dengan fungsi `malloc` dan `free`, `sys/ipc.h` dan `sys/shm.h` digunakan untuk akses ke shared memory pada sistem operasi Unix/Linux, dan `time.h` digunakan untuk menghitung waktu eksekusi program.
    - Konstanta `ROWS1` dan `COLS2` digunakan untuk menentukan ukuran kedua matriks yang akan dikalikan.
- Membuat fungsi faktorial
    
    ```c
    long double factorial(int n) {
       long double result = 1.0L;
       int i;
       for (i = 1; i <= n; i++) {
          result *= (long double)i;
       }
       return result;
    }
    ```
    
    - Fungsi `factorial()` merupakan sebuah fungsi yang digunakan untuk menghitung faktorial dari suatu bilangan bulat n. Faktorial merupakan hasil perkalian semua bilangan asli dari 1 hingga n.
- Attach shared memory
    
    ```c
    int main() {
       int shmid;
       key_t key = 1234;
    
       // Attach shared memory
       int (*shared_result)[COLS2];
       if ((shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666)) < 0) {
          perror("shmget");
          exit(1);
       }
       if ((shared_result = shmat(shmid, NULL, 0)) == (int (*)[COLS2]) -1) {
          perror("shmat");
          exit(1);
       }
    ```
    
    - Variabel `shmid` menyimpan ID dari segmen shared memory yang akan digunakan, sedangkan variabel `key` digunakan sebagai kunci untuk mengakses shared memory.
    - Program melakukan attach pada shared memory yang telah dialokasikan sebelumnya menggunakan fungsi `shmget()` dan `shmat()`. Variabel `shared_result` merupakan pointer ke segmen shared memory yang telah dialokasikan.
        - Fungsi `shmget()` digunakan untuk mendapatkan ID dari segmen shared memory dengan ukuran `sizeof(int[ROWS1][COLS2])` bytes. Apabila alokasi memori gagal, maka program akan menampilkan pesan error dan keluar dari program.
        - Lain halnya dengan fungsi `shmat()` digunakan untuk me-attach segmen shared memory ke dalam alamat virtual space proses saat ini. Apabila proses attach gagal, maka program akan menampilkan pesan error dan keluar dari program.
    
    ```c
    // Tampilkan matriks hasil perkalian
       printf("Matriks hasil perkalian:\n");
       int i, j;
       for (i = 0; i < ROWS1; i++) {
          for (j = 0; j < COLS2; j++) {
             printf("%d ", shared_result[i][j]);
          }
          printf("\n");
       }
    ```
    
    - Pada bagian ini, program menampilkan hasil perkalian kedua matriks yang telah dihitung sebelumnya menggunakan perulangan for. Hasil perkalian disimpan dalam bentuk matriks 2D pada segmen shared memory.
- Proses perhitungan faktorial tanpa thread
    
    ```c
    // Hitung faktorial setiap angka dalam matriks tanpa thread
       long double factorial_value;
       printf("\nHasil faktorial setiap angka dalam matriks (tanpa thread):\n");
       clock_t start = clock();                  // Waktu mulai penghitungan faktorial
       for (i = 0; i < ROWS1; i++) {
          for (j = 0; j < COLS2; j++) {
             factorial_value = factorial(shared_result[i][j]);
             printf("%.0Lf ", factorial_value);
          }
          printf("\n");
       }
       clock_t end = clock();                    // Waktu selesai penghitungan faktorial
       double elapsed_time = (double)(end - start) / CLOCKS_PER_SEC;   // Waktu eksekusi dalam detik
    ```
    
    - Proses penghitungan faktorial dilakukan dengan menggunakan fungsi `factorial()` yang telah ditentukan sebelumnya. Variabel `factorial_value` digunakan untuk menyimpan hasil faktorial dari setiap elemen matriks.
    - Selain itu, program juga menghitung waktu awal sebelum proses perhitungan faktorial dimulai menggunakan fungsi `clock()`, dan menghitung waktu akhir setelah proses perhitungan faktorial selesai. Selisih antara kedua waktu tersebut kemudian dihitung untuk mendapatkan waktu eksekusi dalam detik, dan disimpan pada variabel `elapsed_time`.
    
    ```c
    // Tampilkan waktu eksekusi
       printf("\nWaktu eksekusi (tanpa thread): %.6lf detik\n", elapsed_time);
    ```
    
    - Pada bagian ini, program mencetak waktu eksekusi hasil perhitungan faktorial tanpa menggunakan thread pada layar.
    
    ```c
    // Detach shared memory
       if (shmdt(shared_result) == -1) {
          perror("shmdt");
          exit(1);
       }
    
       return 0;
    }
    ```
    
    - Terakhir, program melepas segmen shared memory yang telah digunakan menggunakan fungsi `shmdt()`. Apabila proses pelepasan shared memory gagal, maka program akan menampilkan pesan error dan keluar dari program.

## Output Soal 1

### **kalian.c**
![Alt Text](https://i.ibb.co/tJmcXKX/Screenshot-2023-05-12-083639.png)

### **kalian.c dan sisop.c**
![Alt Text](https://i.ibb.co/hRhSXNz/Screenshot-2023-05-12-084941.png)

Berdasarkan hasil output yang diberikan, waktu eksekusi program dengan menggunakan thread memerlukan waktu yang lebih lama yaitu 0.001092 detik dibandingkan dengan waktu eksekusi tanpa menggunakan thread yaitu hanya 0.000022 detik. Meskipun pada umumnya penggunaan thread dapat mempercepat waktu eksekusi program, namun dalam kasus ini hal tersebut tidak terjadi. 

Oleh karena itu, dalam beberapa kasus, penggunaan thread tidak selalu menjamin waktu eksekusi yang lebih cepat, dan programmer harus mempertimbangkan kondisi spesifik dan karakteristik dari program mereka sebelum memutuskan apakah thread harus digunakan atau tidak.

## Soal 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.
- Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.
- User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut:
```
└── soal3
	├── playlist.txt
	├── song-playlist.json
	├── stream.c
	└── user.c
```
- Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt
Sample Output:
```
17 - MK
1-800-273-8255 - Logic
1950 - King Princess
…
Your Love Is My Drug - Kesha
YOUTH - Troye Sivan
ZEZE (feat. Travis Scott & Offset) - Kodak Black
```
- User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.
```

PLAY "Stereo Heart" 
    sistem akan menampilkan: 
    USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"
PLAY "BREAK"
    sistem akan menampilkan:
    THERE ARE "N" SONG CONTAINING "BREAK":
    1. THE SCRIPT - BREAKEVEN
    2. ARIANA GRANDE - BREAK FREE
dengan “N” merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":
PLAY "UVUWEVWEVWVE"
    THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"

```
- User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:
- - User mengirimkan perintah
```
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>
```
- - User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”

- Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.
- Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

##### **3**.**1** stream.c 
membuat stream.c berfungsi sebagai sistem penerima (receiver) yang akan menerima perintah dari user.c dan menjalankan aksi yang sesuai dengan perintah yang diterima. Stream.c akan menggunakan message queue untuk berkomunikasi dengan user.c. 
###### **3**.**1**.**1**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <ctype.h>
#include <stdint.h>
#include <jansson.h>
#include <sys/shm.h>
#include <semaphore.h>

```
Bagian ini merupakan header standar C dan library POSIX untuk operasi input-output, operasi string, operasi khusus UNIX, mekanisme IPC (Inter-Process Communication) seperti antrian pesan, memori bersama, semaphore, dan parsing JSON.###

###### **3**.**1**.**2**
```c
#define SHM_KEY 1002
#define MAX_USERS 2
#define MAX_STREAMS 2
#define MAX_MESSAGE_SIZE 1024
#define QUEUE_KEY 1001
```
Bagian ini mendefinisikan beberapa konstanta yang akan digunakan di seluruh program. Ini dilakukan menggunakan preprocessor C #define, yang secara efektif menggantikan setiap kemunculan dari simbol yang ditentukan dengan nilai yang ditentukan sebelum kode dikompilasi.

###### **3**.**1**.**3**
```c
struct message {
    long message_type;
    int user_id;
    char command[MAX_MESSAGE_SIZE];
};
```
Pada bagian ini , struct message mendefinisikan tipe data baru dengan tiga bidang: message_type, user_id, dan command

###### **3**.**1**.**4**
```c
int shm_id;
sem_t user_semaphore;
sem_t playlist_semaphore;
```
pada bagian ini membuat variable global yang akan digunakan
- shm_id: Ini adalah integer yang digunakan untuk menyimpan ID memori bersama (shared memory). Memori bersama adalah cara untuk dua atau lebih proses berbagi sebagian dari memori sistem mereka.
- user_semaphore: Ini adalah semafor untuk mengontrol akses ke jumlah pengguna. Semafor adalah konstruk sinkronisasi yang digunakan untuk melindungi akses ke sumber daya bersama seperti buffer atau antrian. Di sini, sepertinya semafor digunakan untuk membatasi jumlah pengguna yang dapat terhubung ke server pada saat yang sama.
- playlist_semaphore: Ini adalah semafor lain yang digunakan untuk mengontrol akses ke playlist. Sepertinya ini digunakan untuk membatasi jumlah lagu yang dapat diputar secara bersamaan.
###### **3**.**1**.**5**
```c
// Deklarasikan fungsi yang diperlukan di sini
void rot13(char *input, char *output);
void hex_to_str(char *hex, char *str);
void base64_decode(const char *input, char *output);
void sort_playlist(char **playlist, size_t size);
void decrypt_and_sort();
void list_playlist();
void search_and_play(const char *query, int user_id);
void add_song_to_playlist(const char *song_title);
```
Bagian ini mendeklarasikan beberapa fungsi yang akan digunakan dalam program. 

###### **3**.**1**.**6**
```c
int main() {
    int msg_queue_id;
    struct message msg;

    msg_queue_id = msgget(QUEUE_KEY, 0666 | IPC_CREAT);
    if (msg_queue_id == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }
    shm_id = shmget(SHM_KEY, sizeof(int), 0666 | IPC_CREAT);
    if (shm_id == -1) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    int *user_count = (int *)shmat(shm_id, NULL, 0);
    if (user_count == (int *)-1) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    *user_count = 0;
    
      // Inisialisasi semaphore
    if (sem_init(&playlist_semaphore, 0, MAX_STREAMS) == -1) {
    perror("sem_init");
    exit(EXIT_FAILURE);
    }
    if (sem_init(&user_semaphore, 0, MAX_USERS) == -1) {
        perror("sem_init");
        exit(EXIT_FAILURE);
    }

   int *active_users = user_count; // Tambahkan baris ini untuk memperbaiki kesalahan

   while (1) {
        if (msgrcv(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0, 0) == -1) {
            perror("msgrcv");
            exit(EXIT_FAILURE);
        }

        if (strcmp(msg.command, "CONNECT") == 0) {
            if (sem_trywait(&user_semaphore) == 0) {
                (*active_users)++;
                printf("User %d connected\n", msg.user_id);
            } else {
                printf("USER SYSTEM OVERLOAD\n");
            }
        } else if (strcmp(msg.command, "EXIT") == 0) {
            (*active_users)--;
            sem_post(&user_semaphore); // Lepaskan semaphore pengguna
            if (*active_users == 0) {
                break;
            }
        
    } else if (strncmp(msg.command, "DECRYPT", 7) == 0) {
        decrypt_and_sort();
    } else if (strcmp(msg.command, "LIST") == 0) {
        list_playlist();
    } else if (strncmp(msg.command, "PLAY", 4) == 0) {
    if (sem_trywait(&playlist_semaphore) == 0) {
        char query[MAX_MESSAGE_SIZE];
        sscanf(msg.command, "PLAY \"%[^\"]\"", query);
        search_and_play(query, msg.user_id);
    } else {
        printf("STREAM SYSTEM OVERLOAD\n");
    }
    } else if (strncmp(msg.command, "ADD", 3) == 0) {
        char song_title[MAX_MESSAGE_SIZE];
        strncpy(song_title, msg.command + 4, strlen(msg.command) - 4);
        song_title[strlen(msg.command) - 4] = '\0';
        printf("User %d: ADD \"%s\"\n", msg.user_id, song_title);
        add_song_to_playlist(song_title);
    } else {
        printf("UNKNOWN COMMAND\n");
    }
}
    if (sem_destroy(&playlist_semaphore) == -1) {
    perror("sem_destroy");
    exit(EXIT_FAILURE);
    }

    if (msgctl(msg_queue_id, IPC_RMID, NULL) == -1) {
        perror("msgctl");
        exit(EXIT_FAILURE);
    }
    if (shmctl(shm_id, IPC_RMID, NULL) == -1) {
        perror("shmctl");
        exit(EXIT_FAILURE);
    }

    return 0;
}
```
Bagian  ini adalah fungsi utama (main) program. Mari kita lihat apa yang dilakukan oleh masing-masing bagian:

- Membuat antrian pesan dengan kunci QUEUE_KEY dan pembuatan memori bersama dengan kunci SHM_KEY. Jika salah satu operasi ini gagal, program akan mencetak pesan kesalahan dan keluar.

- Menghubungkan memori bersama ke alamat proses menggunakan fungsi shmat. Jika operasi ini gagal, program akan mencetak pesan kesalahan dan keluar. Memori bersama ini digunakan untuk menyimpan jumlah pengguna aktif.

- Menginisialisasi semaphore untuk aliran lagu dan pengguna. Jika operasi ini gagal, program akan mencetak pesan kesalahan dan keluar. Semaphore digunakan untuk membatasi jumlah maksimum aliran lagu dan pengguna.

- Memasuki loop utama yang akan berjalan selamanya. Di dalam loop ini, program akan membaca pesan dari antrian pesan dan memproses perintah dalam pesan tersebut.

- Perintah yang didukung adalah "CONNECT", "EXIT", "DECRYPT", "LIST", "PLAY", dan "ADD". Perintah "CONNECT" dan "EXIT" akan menambahkan dan mengurangi jumlah pengguna aktif. Perintah "DECRYPT" akan mendekripsi daftar lagu. Perintah "LIST" akan menampilkan daftar lagu. Perintah "PLAY" akan mencoba memainkan lagu, dan perintah "ADD" akan mencoba menambahkan lagu ke daftar lagu. Jika perintah tidak dikenal, program akan mencetak "UNKNOWN COMMAND".

- Jika semua pengguna telah keluar (jumlah pengguna aktif menjadi 0), loop utama akan berhenti.

- Setelah loop utama berhenti, program akan menghancurkan semaphore, menghapus antrian pesan, dan melepaskan memori bersama. Jika salah satu operasi ini gagal, program akan mencetak pesan kesalahan dan keluar.

- Akhirnya, program akan mengembalikan 0, yang menandakan bahwa program telah selesai dengan sukses.

###### **3**.**1**.**7**
```c
void rot13(char *input, char *output) {
    for (int i = 0; input[i]; i++) {
        char c = input[i];
        if (isalpha(c)) {
            char offset = isupper(c) ? 'A' : 'a';
            c = ((c - offset + 13) % 26) + offset;
        }
        output[i] = c;
    }
}

```
bagian ini adalah fungsi rot13 melakukan operasi enkripsi sederhana bernama ROT13 pada string input. Dalam operasi ini, setiap huruf dalam string digantikan dengan huruf yang berjarak 13 posisi dari huruf tersebut dalam alfabet. Fungsi ini memeriksa setiap karakter dalam string, dan jika karakter tersebut adalah huruf, ia akan menggantikan huruf tersebut dengan huruf yang berjarak 13 posisi dalam alfabet. Jika karakter bukan huruf, fungsi tidak akan mengubahnya. Hasil operasi ini kemudian disimpan dalam string output.

###### **3**.**1**.**8**
```c
void hex_to_str(char *hex, char *str) {
    size_t len = strlen(hex);
    for (size_t i = 0; i < len; i += 2) {
        sscanf(hex + i, "%2hhx", str + i / 2);
    }
}
```
Bagian ini adalah fungsi hex_to_str mengonversi string heksadesimal ke string biasa. Fungsi ini berjalan melalui string heksadesimal dua karakter pada satu waktu (karena setiap byte diwakili oleh dua digit heksadesimal) dan menggunakan fungsi sscanf untuk mengonversi dua karakter heksadesimal ini menjadi byte tunggal dalam string biasa. Operasi ini diulangi sampai semua karakter dalam string heksadesimal telah diproses, menghasilkan string biasa yang dihasilkan dari konversi heksadesimal ke string karakter biasa.
###### **3**.**1**.**9**
```c
void base64_decode(const char *input, char *output) {
    static const char decoding_table[] = {
        62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1,
        -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
        14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1,
        26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
        44, 45, 46, 47, 48, 49, 50, 51
    };
    size_t input_len = strlen(input);
    size_t output_len = input_len / 4 * 3;
    if (input[input_len - 1] == '=') output_len--;
    if (input[input_len - 2] == '=') output_len--;

    for (size_t i = 0, j = 0; i < input_len;) {
        uint32_t sextet_a = input[i] == '=' ? 0 & i++ : decoding_table[input[i++] - 43];
        uint32_t sextet_b = input[i] == '=' ? 0 & i++ : decoding_table[input[i++] - 43];
        uint32_t sextet_c = input[i] == '=' ? 0 & i++ : decoding_table[input[i++] - 43];
        uint32_t sextet_d = input[i] == '=' ? 0 & i++ : decoding_table[input[i++] - 43];

        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < output_len) output[j++] = (triple >> 2 * 8) & 0xFF;
                if (j < output_len) output[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < output_len) output[j++] = (triple >> 0 * 8) & 0xFF;
    }
    output[output_len] = '\0';
}
```
bagian ini adalah Fungsi base64_decode ini digunakan untuk mendekodekan string yang telah dienkripsi dengan format Base64.

Fungsi ini memulai dengan mendefinisikan tabel decoding yang digunakan untuk memetakan setiap karakter dalam input Base64 ke nilai numerik yang sesuai. Kemudian, fungsi ini menghitung panjang output dengan asumsi setiap 4 karakter Base64 akan menghasilkan 3 byte data asli. Tetapi jika ada karakter '=' di akhir input (yang digunakan sebagai padding dalam encoding Base64), panjang output akan dikurangi sesuai.

Selanjutnya, fungsi ini melakukan loop melalui string input dan mengkonversi setiap 4 karakter Base64 menjadi 3 byte data asli. Proses ini dilakukan dengan mengambil 4 sextet (6 bit) dari tabel decoding, menggabungkannya menjadi satu uint32_t (32 bit), dan kemudian memecahnya menjadi 3 byte.

Terakhir, fungsi ini menambahkan karakter null di akhir output untuk menandai akhir string.

###### **3**.**1**.**10**
```c
void sort_playlist(char **playlist, size_t size) {
// Use bubble sort to sort the playlist in alphabetical order
int swapped;
do {
swapped = 0;
for (size_t i = 0; i < size - 1; i++) {
if (strcmp(playlist[i], playlist[i + 1]) > 0) {
char *temp = playlist[i];
playlist[i] = playlist[i + 1];
playlist[i + 1] = temp;
swapped = 1;
}
}
} while (swapped);
}

```
bagian ini adalah fungsi sort_playlist mengurutkan daftar lagu dalam urutan alfabetis menggunakan metode pengurutan Bubble Sort. Pertama, variabel swapped diinisialisasi. Fungsi ini kemudian memasuki loop do-while, yang akan terus berjalan sampai tidak ada lagi elemen yang dipertukarkan, yang berarti daftar sudah terurut. Dalam setiap iterasi loop, fungsi ini melihat setiap pasangan elemen berurutan dalam array (yang masing-masing mewakili lagu dalam daftar putar). Fungsi strcmp digunakan untuk membandingkan dua string; jika string pertama datang setelah string kedua dalam urutan alfabetis, strcmp akan menghasilkan angka yang lebih besar dari nol. Jika ini adalah kasusnya, dua elemen tersebut dipertukarkan, dan swapped disetel ke 1. Jika tidak ada elemen yang dipertukarkan selama iterasi lengkap, swapped tetap 0 dan loop berakhir, karena daftar putar sekarang dalam urutan alfabetis.

###### **3**.**1**.**11**
```c
void list_playlist() {
    FILE *playlist_file = fopen("playlist.txt", "r");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for reading\n");
        return;
    }

    char line[MAX_MESSAGE_SIZE];
    printf("Playlist:\n");
    while (fgets(line, MAX_MESSAGE_SIZE, playlist_file) != NULL) {
        printf("%s", line);
    }
    fclose(playlist_file);}
```
Pada bagian ini adalah fungsi list_playlist digunakan untuk mencetak daftar lagu dari file "playlist.txt". Pertama, fungsi ini mencoba membuka file "playlist.txt" untuk dibaca dengan fopen. Jika file tidak dapat dibuka (misalnya, jika tidak ada), pesan kesalahan dicetak ke stderr dan fungsi selesai. Jika file dapat dibuka, fungsi ini kemudian mendeklarasikan array karakter 'line' dengan ukuran MAX_MESSAGE_SIZE untuk menyimpan setiap baris dari file. Fungsi kemudian mencetak "Playlist:" ke stdout untuk menandai awal daftar putar. Selanjutnya, fungsi memasuki loop while yang membaca setiap baris dari file "playlist.txt" satu per satu dengan fgets dan mencetaknya. Loop ini berlanjut sampai tidak ada lagi baris yang dapat dibaca, yaitu fgets mengembalikan NULL. Setelah semua baris dicetak, file "playlist.txt" ditutup dengan fclose.

###### **3**.**1**.**12**
```c
void list_playlist() {
    FILE *playlist_file = fopen("playlist.txt", "r");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for reading\n");
        return;
    }

    char line[MAX_MESSAGE_SIZE];
    printf("Playlist:\n");
    while (fgets(line, MAX_MESSAGE_SIZE, playlist_file) != NULL) {
        printf("%s", line);
    }
    fclose(playlist_file);}

void decrypt_and_sort() {
    json_error_t error;
    json_t *root = json_load_file("song-playlist.json", 0, &error);
    if (!root) {
        fprintf(stderr, "Error parsing JSON file: %s\n", error.text);
        return; // Change this line
    }

size_t index;
json_t *data;
char **playlist = malloc(json_array_size(root) * sizeof(char *));
size_t playlist_size = 0;

json_array_foreach(root, index, data) {
    const char *method = json_string_value(json_object_get(data, "method"));
    const char *song = json_string_value(json_object_get(data, "song"));

    char decoded_song[1024] = {0};

    if (strcmp(method, "rot13") == 0) {
        rot13((char *)song, decoded_song);
    } else if (strcmp(method, "hex") == 0) {
        hex_to_str((char *)song, decoded_song);
    } else if (strcmp(method, "base64") == 0) {
        base64_decode(song, decoded_song);
    } else {
        fprintf(stderr, "Unknown method: %s\n", method);
        continue;
    }

    playlist[playlist_size] = strdup(decoded_song);
    playlist_size++;
}

sort_playlist(playlist, playlist_size);

 FILE *playlist_file = fopen("playlist.txt", "w");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for writing\n");
        json_decref(root);
        free(playlist);
        return; // Change this line
    }

for (size_t i = 0; i < playlist_size; i++) {
    fprintf(playlist_file, "%s\n", playlist[i]);
    free(playlist[i]);
}

fclose(playlist_file);
json_decref(root);
free(playlist);
}
```
Fungsi decrypt_and_sort ini digunakan untuk mendekripsi dan mengurutkan daftar putar lagu dari file JSON "song-playlist.json".

- Fungsi ini pertama-tama mencoba membuka dan mengurai file JSON dengan json_load_file. Jika gagal, fungsi ini mencetak pesan kesalahan dan mengembalikan kontrol ke pemanggil.

- Setelah itu, fungsi ini mendeklarasikan pointer ke array playlist dan mengalokasikan memori untuk menyimpan setiap lagu dalam daftar putar.

- Fungsi ini kemudian memasuki loop json_array_foreach yang berjalan melalui setiap elemen dalam array JSON. Untuk setiap elemen, fungsi ini mengambil nilai string dari properti "method" dan "song".

- Fungsi ini kemudian mendeklarasikan array decoded_song untuk menyimpan lagu yang telah didekripsi.

- Menggunakan pernyataan kondisi if, fungsi ini memeriksa metode dekripsi yang digunakan (yaitu, "rot13", "hex", atau "base64") dan memanggil fungsi dekripsi yang sesuai. Jika metode tidak dikenal, fungsi ini mencetak pesan kesalahan dan melanjutkan ke iterasi berikutnya dari loop.

- Fungsi ini kemudian menambahkan lagu yang telah didekripsi ke array playlist dan menambahkan jumlah lagu dalam daftar putar.

-Setelah semua lagu telah didekripsi, fungsi ini mengurutkan lagu dalam playlist dengan fungsi sort_playlist.

- Fungsi ini kemudian mencoba membuka file "playlist.txt" untuk ditulis. Jika gagal, fungsi ini membersihkan semua sumber daya yang dialokasikan dan mengembalikan kontrol ke pemanggil.

- Fungsi ini kemudian memasuki loop for yang menulis setiap lagu dalam playlist ke file "playlist.txt", membebaskan memori yang dialokasikan untuk setiap lagu, dan menutup file "playlist.txt".

- Akhirnya, fungsi ini membersihkan semua sumber daya yang tersisa (yaitu, root dan playlist) sebelum mengakhiri eksekusi.

###### **3**.**1**.**13**
```c
void search_and_play(const char *query, int user_id){
    FILE *playlist_file = fopen("playlist.txt", "r");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for reading\n");
        return;
    }

    // Mengubah query menjadi lowercase
    char query_lower[MAX_MESSAGE_SIZE];
    for (int i = 0; query[i]; i++) {
        query_lower[i] = tolower(query[i]);
    }
    query_lower[strlen(query)] = '\0';

    char line[MAX_MESSAGE_SIZE];
    int match_count = 0;
    size_t line_number = 0;
    char matched_line[MAX_MESSAGE_SIZE];

    printf("Searching for \"%s\"...\n", query_lower);
    while (fgets(line, MAX_MESSAGE_SIZE, playlist_file) != NULL) {
        line_number++;

        // Mengubah judul lagu menjadi lowercase
        char line_lower[MAX_MESSAGE_SIZE];
        for (int i = 0; line[i]; i++) {
            line_lower[i] = tolower(line[i]);
        }
        line_lower[strlen(line)] = '\0';

        if (strstr(line_lower, query_lower) != NULL) {
            match_count++;
            strncpy(matched_line, line, MAX_MESSAGE_SIZE);
            printf("%zu. %s", line_number, line);
        }
    }

    int *active_users = (int *)shmat(shm_id, NULL, 0);
    if (active_users == (int *)-1) {
        perror("shmat");
        return;
    }

    if (match_count == 0) {
        printf("There is no song containing \"%s\"\n", query_lower);
    } else if (match_count == 1) {
    printf("User %d playing \"%s\"", user_id, matched_line);
    sleep(5); // Simulasikan durasi lagu, gantilah dengan kode yang sebenarnya untuk memutar lagu
    sem_post(&playlist_semaphore);  // Lepaskan semaphore
    }else {
        printf("There are %d songs containing \"%s\":\n", match_count, query_lower);
    }

    if (shmdt(active_users) == -1) {
        perror("shmdt");
        return;
    }

    fclose(playlist_file);
}
```
Fungsi search_and_play ini digunakan untuk mencari lagu berdasarkan query dalam file playlist dan memutarnya.

- Fungsi ini pertama kali mencoba membuka file "playlist.txt" untuk dibaca. Jika gagal, fungsi ini mencetak pesan kesalahan dan mengembalikan kontrol ke pemanggil.
- Kemudian fungsi ini mengubah query pencarian menjadi huruf kecil dan menyimpannya dalam query_lower.
- Fungsi ini kemudian mendeklarasikan variabel line, match_count, line_number, dan matched_line yang digunakan untuk membaca setiap baris dari file, menghitung jumlah lagu yang cocok, menyimpan nomor baris, dan menyimpan baris yang cocok.
- Fungsi ini kemudian mencetak pesan yang mengatakan bahwa pencarian telah dimulai.
- Fungsi ini kemudian memasuki loop while yang berjalan melalui setiap baris dalam file "playlist.txt". Untuk setiap baris, fungsi ini mengubah baris menjadi huruf kecil dan mencocokkan baris dengan query. Jika ada kecocokan, fungsi ini menambah jumlah kecocokan, menyalin baris ke matched_line, dan mencetak baris.
- Fungsi ini kemudian mencoba mengakses shared memory yang berisi daftar pengguna aktif. Jika gagal, fungsi ini mencetak pesan kesalahan dan mengembalikan kontrol ke pemanggil.
- Jika tidak ada kecocokan, fungsi ini mencetak pesan bahwa tidak ada lagu yang cocok. Jika ada satu kecocokan, fungsi ini mencetak pesan bahwa pengguna memutar lagu, menunggu selama 5 detik (yang meniru durasi lagu), dan melepaskan semaphore. Jika ada lebih dari satu kecocokan, fungsi ini mencetak jumlah lagu yang cocok.
- Fungsi ini kemudian mencoba melepaskan shared memory. Jika gagal, fungsi ini mencetak pesan kesalahan.
- Akhirnya, fungsi ini menutup file "playlist.txt" dan mengakhiri eksekusi.

###### **3**.**1**.**14**
```c
void add_song_to_playlist(const char *song_title) {
    FILE *playlist_file = fopen("playlist.txt", "a");
    if (!playlist_file) {
        fprintf(stderr, "Error opening playlist.txt for appending\n");
        return;
    }

    fprintf(playlist_file, "%s\n", song_title);
    fclose(playlist_file);
}
```
Fungsi add_song_to_playlist dirancang untuk menambahkan judul lagu ke file playlist. Fungsi ini dimulai dengan mencoba membuka file "playlist.txt" dalam mode tambah ('a'). Jika file gagal dibuka, pesan kesalahan akan dicetak dan fungsi akan segera kembali, menghentikan eksekusi lebih lanjut.

Jika file berhasil dibuka, fungsi ini melanjutkan untuk menulis judul lagu ke file "playlist.txt". Hal ini dilakukan dengan menambahkan karakter baris baru ('\n') di akhir, memastikan bahwa setiap judul lagu akan menduduki barisnya sendiri di file.

Setelah judul lagu berhasil ditulis ke file, fungsi menutup file "playlist.txt". Menutup file adalah langkah penting untuk memastikan semua operasi penulisan selesai dan sumber daya apa pun yang digunakan oleh file dibebaskan kembali ke sistem.

##### **3**.**2** user.c 
membuat user.c berfungsi sebagai pengirim perintah ke sistem stream.c. User.c akan menggunakan message queue untuk mengirim perintah kepada stream.c

###### **3**.**2**.**1**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
```
Baris kode ini merupakan direktif #include yang digunakan untuk memasukkan header file ke dalam kode sumber. Header file ini berisi deklarasi fungsi, tipe data, dan konstanta yang akan digunakan dalam program

###### **3**.**2**.**2**
```c
#define QUEUE_KEY 1001
#define MAX_MESSAGE_SIZE 1024
```
Bagian ini mendefinisikan beberapa konstanta yang akan digunakan di seluruh program. Ini dilakukan menggunakan preprocessor C #define, yang secara efektif menggantikan setiap kemunculan dari simbol yang ditentukan dengan nilai yang ditentukan sebelum kode dikompilasi
###### **3**.**2**.**3**
```c
struct message {
    long message_type;
    int user_id;
    char command[MAX_MESSAGE_SIZE];
};

```
Pada bagian ini , struct message mendefinisikan tipe data baru dengan tiga bidang: message_type, user_id, dan command
###### **3**.**2**.**4**

```c
int main() {
    int msg_queue_id;
    struct message msg;

    msg_queue_id = msgget(QUEUE_KEY, 0666 | IPC_CREAT);
    if (msg_queue_id == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    printf("Enter your user ID: ");
    scanf("%d", &msg.user_id);

    msg.message_type = 1;
    strcpy(msg.command, "CONNECT");
    if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) {
        perror("msgsnd");
        exit(EXIT_FAILURE);
    }

    while (1) {
        printf("> ");
        char command[MAX_MESSAGE_SIZE];
        scanf(" %[^\n]", command);

        if (strcmp(command, "EXIT") == 0) {
            strcpy(msg.command, "EXIT");
            if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) {
                perror("msgsnd");
                exit(EXIT_FAILURE);
            }
            break;
        } else {
            strncpy(msg.command, command, MAX_MESSAGE_SIZE);
            if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) {
                perror("msgsnd");
                exit(EXIT_FAILURE);
            }
        }
    }

    return 0;
}

```
Kode di atas merupakan implementasi dari fungsi main dalam program. Berikut adalah penjelasan per baris:

1. int msg_queue_id; Mendeklarasikan variabel msg_queue_id bertipe int untuk menyimpan identifikasi antrian pesan.
2. struct message msg; Mendeklarasikan variabel msg bertipe struct message untuk menyimpan pesan yang akan dikirim melalui antrian pesan.
3. msg_queue_id = msgget(QUEUE_KEY, 0666 | IPC_CREAT); Mencoba mendapatkan akses ke antrian pesan dengan menggunakan fungsi msgget. Jika antrian pesan dengan kunci QUEUE_KEY sudah ada, akan mengembalikan identifikasi antrian tersebut. Jika tidak, akan membuat antrian baru dengan kunci tersebut.
4. if (msg_queue_id == -1) { ... } Memeriksa apakah operasi msgget berhasil atau gagal. Jika gagal, maka menampilkan pesan kesalahan menggunakan perror dan keluar dari program dengan exit(EXIT_FAILURE).
5. printf("Enter your user ID: "); Menampilkan pesan untuk meminta pengguna memasukkan ID pengguna.
6. scanf("%d", &msg.user_id); Membaca input ID pengguna yang dimasukkan oleh pengguna dan menyimpannya di dalam variabel msg.user_id.
7. msg.message_type = 1; Menetapkan nilai 1 ke message_type dalam variabel msg. Nilai ini menentukan tipe pesan yang akan dikirim melalui antrian pesan.
8. strcpy(msg.command, "CONNECT"); Menyalin string "CONNECT" ke dalam command dalam variabel msg. Ini menunjukkan perintah yang akan dikirim ke server.
9. if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) { ... } Mengirimkan pesan ke antrian pesan menggunakan fungsi msgsnd. Pesan yang dikirim adalah msg dengan ukuran sesuai dengan sizeof(struct message) - sizeof(long). Jika pengiriman gagal, menampilkan pesan kesalahan menggunakan perror dan keluar dari program.
10. while (1) { ... } Memulai loop tak terbatas untuk menerima input perintah dari pengguna dan mengirimkannya ke antrian pesan.
11. printf("> "); Menampilkan prompt untuk meminta pengguna memasukkan perintah.
12. char command[MAX_MESSAGE_SIZE]; Mendeklarasikan array karakter command dengan ukuran MAX_MESSAGE_SIZE untuk menyimpan perintah yang dimasukkan oleh pengguna.
13. scanf(" %[^\n]", command); Membaca input perintah yang dimasukkan oleh pengguna dan menyimpannya di dalam command. Format "%[^\n]" digunakan untuk membaca seluruh baris perintah yang dimasukkan oleh pengguna, termasuk spasi.
14. if (strcmp(command, "EXIT") == 0) { ... } Memeriksa apakah perintah yang dimasukkan adalah "EXIT". Jika ya, maka mengubah msg.command menjadi "EXIT" dan mengirimkan pesan ke antrian pesan dengan menggunakan msgsnd
15. strcpy(msg.command, "EXIT"); Menyalin string "EXIT" ke dalam command dalam variabel msg. Ini menunjukkan perintah "EXIT" yang akan dikirim ke server.
16. if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) { ... } Mengirimkan pesan dengan perintah "EXIT" ke antrian pesan menggunakan fungsi msgsnd. Jika pengiriman gagal, menampilkan pesan kesalahan menggunakan perror dan keluar dari program.
17. break; Keluar dari loop tak terbatas, sehingga program selesai.
18. Jika perintah bukan "EXIT", maka perintah yang dimasukkan oleh pengguna disalin ke msg.command dengan menggunakan strncpy. Hal ini memastikan bahwa perintah yang melebihi ukuran maksimum MAX_MESSAGE_SIZE tidak melampaui batas array.
19. if (msgsnd(msg_queue_id, &msg, sizeof(struct message) - sizeof(long), 0) == -1) { ... } Mengirimkan pesan dengan perintah yang dimasukkan oleh pengguna ke antrian pesan menggunakan fungsi msgsnd. Jika pengiriman gagal, menampilkan pesan kesalahan menggunakan perror dan keluar dari program.
20. Setelah loop selesai, program mengembalikan nilai 0, menandakan bahwa program selesai dengan sukses.

## Output Soal 3

### **Jalankan program**
![Alt Text](https://i.ibb.co/FJQKGjk/Screenshot-2023-05-13-202407.png)
### **Decrypt**
![Alt Text](https://i.ibb.co/3dcpNyK/Screenshot-2023-05-13-203317.png)
### **LIST**
<a href="https://ibb.co/sH9Gt77"><img src="https://i.ibb.co/jRk2Htt/Screenshot-82.png" alt="Screenshot-82" border="0"></a>
### **PLAY**
<a href="https://ibb.co/R0PN7zn"><img src="https://i.ibb.co/0MZKQhN/Screenshot-83.png" alt="Screenshot-83" border="0"></a>
<a href="https://ibb.co/VDgBBpD"><img src="https://i.ibb.co/bLsddRL/Screenshot-87.png" alt="Screenshot-87" border="0"></a>
<a href="https://ibb.co/vxwftzs"><img src="https://i.ibb.co/4ZmCQNs/Screenshot-88.png" alt="Screenshot-88" border="0"></a>

### **ADD**
<a href="https://ibb.co/8gc3hTs"><img src="https://i.ibb.co/YBZ9rYQ/Screenshot-84.png" alt="Screenshot-84" border="0"></a>
<a href="https://ibb.co/4tWHqWn"><img src="https://i.ibb.co/MSnmvnx/Screenshot-85.png" alt="Screenshot-85" border="0"></a>

### **UNKNOWN COMMAND**
<a href="https://ibb.co/cw4bH1z"><img src="https://i.ibb.co/8b1zp0F/Screenshot-86.png" alt="Screenshot-86" border="0"></a>
### **SYSTEM OVERLOAD**
<a href="https://ibb.co/jGg7p2z"><img src="https://i.ibb.co/1K8HS5M/Screenshot-89.png" alt="Screenshot-89" border="0"></a>
<a href="https://ibb.co/jbZg1kr"><img src="https://i.ibb.co/2WYgf3k/Screenshot-90.png" alt="Screenshot-90" border="0"></a>


# Laporan Praktikum Sistem Operasi - Modul 2

## Soal 4

Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu *file* bernama **[hehe.zip](https://drive.google.com/file/d/1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp/view?usp=sharing)**. Di dalam *file* .zip tersebut, terdapat sebuah folder bernama **files** dan *file* .txt bernama **extensions.txt** dan **max.txt**. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan *file* tersebut!

1. *Download* dan *unzip file* tersebut dalam kode c bernama **unzip.c**.
2. Selanjutnya, buatlah program **categorize.c** untuk mengumpulkan (move / copy) *file* sesuai *extension*nya. *Extension* yang ingin dikumpulkan terdapat dalam *file* **extensions.txt**. Buatlah folder **categorized** dan di dalamnya setiap *extension* akan dibuatkan *folder* dengan nama sesuai nama *extension*nya dengan nama *folder* semua *lowercase*. Akan tetapi, *file* bisa saja tidak semua *lowercase*. *File* lain dengan *extension* selain yang terdapat dalam .txt *files* tersebut akan dimasukkan ke *folder* **other**. Pada *file* **max.txt**, terdapat angka yang merupakan isi maksimum dari *folder* tiap *extension* kecuali *folder* **other**. Sehingga, jika penuh, buatlah *folder* baru dengan format **extension (2)**, **extension (3)**, dan seterusnya.
3. *Output*kan pada *terminal* banyaknya *file* tiap *extension* terurut *ascending* dengan semua *lowercase*, beserta **other** juga dengan format sebagai berikut.
    
    **extension_a : banyak_file**
    
    **extension_b : banyak_file**
    
    **extension_c : banyak_file**
    
    **other : banyak_file**
    
4. Setiap pengaksesan *folder*, *sub**folder*, dan semua *folder pada program categorize.c* **wajib menggunakan** ***multithreading***. Jika tidak menggunakan akan ada pengurangan nilai.

### 4**.1**

Kami diminta untuk membuat program untuk *Download* dan *unzip file* tersebut dalam kode c bernama **unzip.c**.

- Import beberapa library dan pendefinisian beberapa konstanta.
    
    ```c
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    ```
    
    - Pada bagian ini untuk menjalankan fungsi-fungsi yang digunakan dalam program. **`stdlib.h`** diperlukan untuk menggunakan fungsi **`system()`** dan **`sprintf()`**, **`stdio.h`** diperlukan untuk menggunakan fungsi **`printf()`**, dan **`string.h`** diperlukan untuk menggunakan fungsi **`strcpy()`** dan **`strlen()`**.
- Proses Download
    - 
        
        ```c
        int main() {
        	// set the download URL and output filename
        	char url[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
        	char outfilename[] = "hehe.zip";
        ```
        
        - Pada bagian ini berfungsi menentukan URL (alamat) dari file yang akan didownload dan nama file yang akan diberikan pada file hasil download tersebut.
        
        ```c
        // build the command to download the file using wget
        char command[1000];
        sprintf(command, "wget '%s' -O %s", url, outfilename);
        // execute the command
        system(command);
        ```
        
        - Perintah untuk mendownload file menggunakan perintah **`wget`**. Perintah tersebut disimpan dalam variabel **`command`** yang telah dideklarasikan sebelumnya. Fungsi **`sprintf()`** digunakan untuk memformat perintah **`wget`** dengan menambahkan nilai dari variabel **`url`** dan **`outfilename`** ke dalam perintah.
- Proses Unzip
    
    ```c
    // build the command to extract the zip file in the current directory
    sprintf(command, "unzip %s", outfilename);
    // execute the command
    system(command);
    ```
    
    - Perintah untuk mengekstrak file zip pada direktori tempat program dijalankan. Perintah tersebut disimpan dalam variabel **`command`** yang telah dideklarasikan sebelumnya. Fungsi **`sprintf()`** digunakan untuk memformat perintah **`unzip`** dengan menambahkan nama file hasil download dari variabel **`outfilename`** ke dalam perintah.

## Output Soal 4

### **Unzip.c**
![Alt Text](https://i.ibb.co/PhFWbsS/Screenshot-2023-05-13-212412.png)


