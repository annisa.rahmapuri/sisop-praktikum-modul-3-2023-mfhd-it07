#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


#define MAX_SIZE 256

// Structure for a node in Huffman Tree
typedef struct Node {
    char character;
    int frequency;
    struct Node* left;
    struct Node* right;
} Node;

// Function to count the frequency of each character in the file
void countCharacterFrequency(FILE* file, int frequencies[]) {
    int ch;
    while ((ch = fgetc(file)) != EOF) {
        if (ch >= 'A' && ch <= 'Z') {
            frequencies[ch]++;
        }
    }
}

// Function to create a new node for Huffman Tree
Node* createNode(char character, int frequency) {
    Node* node = (Node*)malloc(sizeof(Node));
    node->character = character;
    node->frequency = frequency;
    node->left = NULL;
    node->right = NULL;
    return node;
}

// Function to build Huffman Tree from character frequencies
Node* buildHuffmanTree(int frequencies[]) {
    // Create a min-heap (priority queue) of nodes
    Node* heap[MAX_SIZE];
    int heapSize = 0;

    for (int i = 0; i < MAX_SIZE; i++) {
        if (frequencies[i] > 0) {
            heap[heapSize++] = createNode(i, frequencies[i]);
        }
    }

    // Build the Huffman Tree
    while (heapSize > 1) {
        // Extract the two nodes with minimum frequency
        Node* left = heap[--heapSize];
        Node* right = heap[--heapSize];

        // Create a new internal node with combined frequency
        Node* internalNode = createNode('\0', left->frequency + right->frequency);
        internalNode->left = left;
        internalNode->right = right;

        // Insert the new node back into the min-heap
        int i = heapSize;
        while (i > 0 && internalNode->frequency < heap[i - 1]->frequency) {
            heap[i] = heap[i - 1];
            i--;
        }
        heap[i] = internalNode;
        heapSize++;
    }

    // Return the root of the Huffman Tree
    return heap[0];
}

// Function to write Huffman Tree to compressed file in text format
void writeHuffmanTreeToFile(Node* root, FILE* file) {
    if (root == NULL)
        return;

    if (root->left == NULL && root->right == NULL) {
        fprintf(file, "1%c", root->character);  // Leaf node indicator
    } else {
        fprintf(file, "0");  // Internal node indicator
        writeHuffmanTreeToFile(root->left, file);
        writeHuffmanTreeToFile(root->right, file);
    }
}

// Function to convert character to Huffman code and send through pipe in text format
void convertToHuffmanCode(Node* root, char character, char* code, int depth, FILE* pipeWrite) {
    if (root == NULL)
        return;

        if (root->left == NULL && root->right == NULL && root->character == character) {
        code[depth] = '\0';  // Null-terminate the code
        fprintf(pipeWrite, "%s", code);  // Send Huffman code through pipe
        return;
    }

    code[depth] = '0';
    convertToHuffmanCode(root->left, character, code, depth + 1, pipeWrite);

    code[depth] = '1';
    convertToHuffmanCode(root->right, character, code, depth + 1, pipeWrite);
}

// Function to count the number of bits in a file (in text format)
int countBitsInFile(FILE* file) {
    int count = 0;
    int ch;

    while ((ch = fgetc(file)) != EOF) {
        count += 8;  // Assuming 8 bits per character
    }

    return count;
}

// Function to count the number of bits in Huffman-compressed file (in text format)
int countBitsInCompressedFile(FILE* file) {
    int count = 0;
    char code[MAX_SIZE];
    int depth = 0;
    int ch;

    while ((ch = fgetc(file)) != EOF) {
        if (ch == '0' || ch == '1') {
            code[depth++] = ch;
        } else if (ch == '\n') {
            code[depth] = '\0';  // Null-terminate the code
            count += strlen(code);
            depth = 0;
        }
    }

    return count;
}

int main() {
    int frequencies[MAX_SIZE] = {0};
    FILE* file = fopen("file.txt", "r");

    if (file == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    // Count character frequencies
    countCharacterFrequency(file, frequencies);

    // Build Huffman Tree
    Node* huffmanTree = buildHuffmanTree(frequencies);

    // Create a pipe
    int pipefd[2];
    if (pipe(pipefd) == -1) {
        printf("Error creating pipe.\n");
        return 1;
    }

    // Fork a child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error forking process.\n");
        return 1;
    } else if (pid == 0) {
        // Child process
        // Close the read end of the pipe
        close(pipefd[0]);

        // Write Huffman Tree to compressed file
        FILE* compressedFileWrite = fopen("compressed.txt", "w");
        if (compressedFileWrite == NULL) {
            printf("Error opening compressed file for writing.\n");
            return 1;
        }
        writeHuffmanTreeToFile(huffmanTree, compressedFileWrite);
        fclose(compressedFileWrite);

        // Convert characters to Huffman codes and send through pipe
        char* code = (char*)malloc(MAX_SIZE * sizeof(char));
        FILE* pipeWrite = fdopen(pipefd[1], "w");
        if (pipeWrite == NULL) {
            printf("Error opening pipe for writing.\n");
            return 1;
        }
                for (int i = 0; i < MAX_SIZE; i++) {
            if (frequencies[i] > 0) {
                convertToHuffmanCode(huffmanTree, i, code, 0, pipeWrite);
            }
        }
        fclose(pipeWrite);

        // Wait for the child process to finish
        wait(NULL);

        // Read Huffman Tree from compressed file
        FILE* compressedFileRead = fopen("compressed.txt", "r");
        if (compressedFileRead == NULL) {
            printf("Error opening compressed file for reading.\n");
            return 1;
        }
        //Node* decompressedHuffmanTree = buildHuffmanTreeFromFile(compressedFileRead);
        fclose(compressedFileRead);

        // Open the compressed file for reading
        FILE* compressedFileRead2 = fopen("compressed.txt", "r");
        if (compressedFileRead2 == NULL) {
            printf("Error opening compressed file for reading.\n");
            return 1;
        }

        // Perform decompression and write to the decompressed file
        FILE* decompressedFile = fopen("decompressed.txt", "w");
        if (decompressedFile == NULL) {
            printf("Error opening decompressed file for writing.\n");
            return 1;
        }
        //decompressFile(compressedFileRead2, decompressedFile, decompressedHuffmanTree);
        fclose(compressedFileRead2);
        fclose(decompressedFile);

         // Count the number of bits in the original file
        FILE* originalFile = fopen("file.txt", "r");
        if (originalFile == NULL) {
            printf("Error opening file.\n");
            return 1;
        }
        int originalBits = countBitsInFile(originalFile);
        fclose(originalFile);

        // Count the number of bits in the compressed file
        FILE* compressedFileCount = fopen("compressed.txt", "r");
        if (compressedFileCount == NULL) {
            printf("Error opening compressed file for reading.\n");
            return 1;
        }
        int compressedBits = countBitsInCompressedFile(compressedFileCount);
        fclose(compressedFileCount);

        int compressionRatio = (int)(((float)compressedBits / originalBits) * 100);
        
        printf("Number of bits before compression: %d\n", originalBits);
        printf("Number of bits after compression: %d\n", compressedBits);
        printf("Compression ratio: %d%%\n", compressionRatio);
        fflush(stdout);

        return 0;
    }

    return 0;
}

